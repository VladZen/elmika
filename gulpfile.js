var gulp = require('gulp'),
	connect = require('gulp-connect'),
	compass = require('gulp-compass'),
	plumber = require('gulp-plumber'),
	autoprefixer = require('gulp-autoprefixer'),
	livereload = require('gulp-livereload'),
	notify = require('gulp-notify'),
	imagemin = require('gulp-imagemin'),
	newer = require('gulp-newer'),
	fileinclude  = require('gulp-file-include');

gulp.task('connect', function() {
	connect.server({
		root: 'build/',
		livereload: true
	});
});

gulp.task('css',function () {
	gulp.src('assets/scss/*.scss')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(compass({
			css: 'build/css',
			sass: 'assets/scss/',
			image: 'build/img/',
			logging: true,
			relative: true,
			sourcemap: false,
			time: true,
			debug: false,
			generated_images_path: 'build/img/'
		}))
		.pipe(autoprefixer({
			browsers: ["last 20 versions", "> 1%", "ie 8", "Firefox <= 20", "Opera 12.1"],
			cascade: false,
			remove: false
		}))
		.pipe(gulp.dest('build/css'))
		.pipe(notify('Css task complete!'))
		.pipe(connect.reload());
});

gulp.task('html', function () {
	gulp.src('assets/html/*.html')
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest('build'))
		.pipe(notify('Html task complete!'))
		.pipe(connect.reload());
});

gulp.task('js', function () {
	gulp.src('build/js/*.js')
		.pipe(notify('Js-files updated!'))
		.pipe(connect.reload());
});

gulp.task('img', function () {
	gulp.src('assets/img/**/*')
		.pipe(newer('build/img/'))
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest('build/img/'))
		.pipe(connect.reload());
});

gulp.task('pic', function () {
	gulp.src('assets/pic/**/*')
		.pipe(newer('build/pic/'))
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest('build/pic/'))
		.pipe(connect.reload());
});

gulp.task('watch',function () {
	gulp.watch('assets/scss/**/*.scss',['css'])
	gulp.watch('assets/html/**/*.html',['html'])
	gulp.watch('assets/img/**/*',['img'])
	gulp.watch('assets/pic/**/*',['pic'])
	gulp.watch('build/js/*.js',['js'])
});

gulp.task('default', ['connect', 'css', 'html', 'js', 'watch']);