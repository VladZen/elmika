//plugin init block
if (typeof ($.fn.placeholder) != 'undefined'){
	$('input, textarea').placeholder();
}

if (typeof ($.fn.Chocolat) != 'undefined'){
	$('.js-zoom').Chocolat();
}

if (typeof ($.fn.slick) != 'undefined'){
	 $('.item-card__slider-top').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		swipe: false,
		infinite: true,
		fade: true,
		speed: 500
	});
	$('.item-card__slider-bottom').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.item-card__slider-top',
		centerMode: true,
		focusOnSelect: true,
		arrows:true,
		variableWidth: true,
		speed: 500
	});
	$('.cat-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		vertical: true,
		arrows:true,
		autoplay: true,
  		autoplaySpeed: 3000,
  		swipe: false,
	});
	$(window).on('load', function(){
		var $self = $('.cat-slider'),
			$total = $('.cat-slider .cat-slider__slide').filter(':not(.slick-cloned)').length;
		$self.append('<div class="cat-slider__pages"><span class="cat-slider__pages__current">1</span> / <span class="cat-slider__pages__total">'+$total+'</span></div>');
	});
	$('.cat-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var $text = $('.cat-slider__slide__title');
 		$text.fadeOut(200);
	});
	$('.cat-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
		var $currentSlide = currentSlide + 1,
			$text = $('.cat-slider__slide__title'),
			$currentPlaceholder = $('.cat-slider__pages__current');
 		$currentPlaceholder.text($currentSlide);
 		$text.fadeIn(200);
	});
	
}

$(window).on('load', function(){
	var $breadcrumbs = $('.breadcrumbs__list__item'),
		$breadcrumbsTotal = $breadcrumbs.length,
		$breadcrumbsList = $('.breadcrumbs__list');
	if($breadcrumbsTotal > 3){
		$breadcrumbs.eq(1).addClass('js-dotted');
	}
	$breadcrumbsList.fadeIn(500);
});


//header style block
(function HeaderStyle(){
	'use strict';

	var $submenu = $('.header__subnav-list'),
		$searchForm = $('#search-form'),
		$submenus = [],
		$searchHeight = 62,//высота поиска (задается кастомно)
		$submenuHeight;

	$(window).on('load', function(){ //Chrome fix height counting
		$submenu.each(function(){
			$submenus.push($(this).outerHeight());
		});		
		$submenuHeight = Math.max.apply(Math, $submenus) + 25; //find the largest submenu height + little offset 25px
	});
	
	var $overlay = {
		obj: $('#header-overlay'),
		trigger: $('.js-overlay-trigger'),
		shownEl: 'overlayed',
		closeBtn: $('#header-overlay-close'),
		show: function(callback){
			$overlay.obj.slideDown(400, callback);
		},
		hide: function(){
			$overlay.beforeHide($('.'+ $overlay.shownEl), function(){
				$overlay.obj.slideUp(400);
				$overlay.trigger.removeClass('active');
			});
		},
		afterShow: function(el,callback){
			el.addClass($overlay.shownEl).fadeIn(200, callback);		
		},
		beforeHide: function(el,callback){
			el.removeClass($overlay.shownEl).fadeOut(100, callback);
		},
		wayOfSliding: function(height,somethingToShow){ //собственно логика
			if ($overlay.obj.is(':hidden')) {
				$(this).addClass('active');
				$overlay.obj.height(height);
				$overlay.show(function(){
					$overlay.afterShow(somethingToShow)
				});
			} else {
				$overlay.trigger.removeClass('active');
				$(this).addClass('active');
				$('.'+$overlay.shownEl).fadeOut(50,function(){
					$overlay.obj.animate({'height':height},400, function(){
						$overlay.afterShow(somethingToShow);
					});
				});	
			}
		},
		behavior: function() {
			$overlay.trigger.on('click', function(event){
				event.preventDefault();
				event.stopPropagation();
				if (!$(this).hasClass('active')) {
					var $submenu = $(this).siblings('.header__subnav-list');
					switch($submenu.length > 0) { 
						case true: // если возле триггера, есть сабменю, то выводим его и используем высоту самого большого меню
							$overlay.wayOfSliding.call(this,$submenuHeight,$submenu);
							break;
						case false: // если нет, то показываем поиск и используем высоту формы поиска
							$overlay.wayOfSliding.call(this,$searchHeight,$searchForm);
							break;
					}
				} else {
					$overlay.hide();
				}	
			});
			$overlay.closeBtn.on('click', function () {
				$overlay.hide();
			});
			$(document).on('click', function (event) {
				console.log(event.target);
				if(!$(event.target).is('.header__subnav-list__item,.header__subnav-list,.header__overlay,.header__search__input,.header__search__submit,header__search__form,.header__search')){
					$overlay.hide();
				}
			});
		}
	}

	$overlay.behavior();

})();

//main page history animation
(function MpHistory(){
	'use strict';

	var $history = {
		block: document.getElementsByClassName('js-history'),
		currentScrolled: function(){
			var $scrolled = window.pageYOffset || document.documentElement.scrollTop;
			return $scrolled;
		},
		mechanism: function(coor){
			if(coor > 300){
				for (var i = 0; i < 5; i++) {
					$('.js-history:nth-child('+i+')').delay(500*i).fadeIn(1000);		
				}
			}
		},
		appear: function(){
			$(window).on('scroll', function (event) {
				if($('.js-history').is(':hidden')){	
					$history.mechanism($history.currentScrolled());
				}	
			});
			
		}
	};

	$history.appear();
	
})();